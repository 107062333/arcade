const {ccclass, property} = cc._decorator;

@ccclass
export default class boomber extends cc.Component 
{

    private anim = null; //this will use to get animation component

    private animateState = null; //this will use to record animationState


    private bulletPool = null; // this is a bullet manager, and it control the bullet resource

    private playerSpeed_x: number = 0;
    private playerSpeed_y: number = 0;

    private aDown: boolean = false; // key for player to go left

    private dDown: boolean = false; // key for player to go right

    private wDown: boolean = false; // key for player to shoot

    private sDown: boolean = false; // key for player to jump

    private qDown: boolean = false; // key for player to jump
    private eDown: boolean = false;
    public disappear: boolean = false;

    private preDown_move: string = null;

    private onGround: boolean = false;

    private isDead: boolean = true;

    onLoad()
    {
        // ===================== TODO =====================
        // 1. Use "this.anim" to record Animation component
        // ================================================
        this.anim = this.getComponent(cc.Animation);
    }

    start() 
    {
        // add key down and key up event
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
        this.animateState = this.anim.play("boomber");
    }

    onKeyDown(event) 
    {
        switch(event.keyCode) 
        {
            case cc.macro.KEY.q:

                this.qDown = true;

                break;
            case cc.macro.KEY.e:

                this.eDown = true;

                break;
        }
    }

    onKeyUp(event)
    {
        switch(event.keyCode) 
        {
            case cc.macro.KEY.q:

                this.qDown = false;

                break;
            case cc.macro.KEY.e:

                this.eDown = false;

                break;
        }
    }
    private playerMovement(dt)
    {   
        if(this.disappear)return;
        if(this.qDown){
            if(this.animateState.name!='boom')this.animateState = this.anim.play('boom');
            this.disappear = true;
        }
        else if(this.eDown){
            if(this.animateState.name!='boom2')this.animateState = this.anim.play('boom2');
            this.disappear = true;
        }
        if(this.disappear){
            this.scheduleOnce(function(){
                this.node.destroy();
            },0.8)
        }
    }
    update(dt)
    {
        this.playerMovement(dt);
    }
}