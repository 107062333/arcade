const {ccclass, property} = cc._decorator;

@ccclass
export default class evie extends cc.Component 
{

    private anim = null; //this will use to get animation component

    private animateState = null; //this will use to record animationState


    private bulletPool = null; // this is a bullet manager, and it control the bullet resource

    private playerSpeed_x: number = 0;
    private playerSpeed_y: number = 0;

    private aDown: boolean = false; // key for player to go left

    private dDown: boolean = false; // key for player to go right

    private wDown: boolean = false; // key for player to shoot

    private sDown: boolean = false; // key for player to jump

    private preDown_move: string = null;

    private onGround: boolean = false;

    private isDead: boolean = true;

    onLoad()
    {
        // ===================== TODO =====================
        // 1. Use "this.anim" to record Animation component
        // ================================================
        this.anim = this.getComponent(cc.Animation);
    }

    start() 
    {
        // add key down and key up event
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);

        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
        this.animateState = this.anim.play("ive_idle_after_backward");
    }

    onKeyDown(event) 
    {
        switch(event.keyCode) 
        {
            case cc.macro.KEY.a:

                this.aDown = true;

                this.dDown = false;

                break;

            case cc.macro.KEY.d:

                this.dDown = true;

                this.aDown = false;

                break;

            case cc.macro.KEY.w:

                this.wDown = true;

                break;

            case cc.macro.KEY.s:

                this.sDown = true;

                break;
        }
    }

    onKeyUp(event)
    {
        switch(event.keyCode) 
        {
            case cc.macro.KEY.a:

                this.aDown = false;

                break;

            case cc.macro.KEY.d:

                this.dDown = false;

                break;

            case cc.macro.KEY.w:

                this.wDown = false;

                break;

            case cc.macro.KEY.s:

                this.sDown = false;

                break;
        }
    }
    private playerMovement(dt)
    {
        if(this.aDown){
            this.playerSpeed_x = -300;
            this.playerSpeed_y = 0;
            this.preDown_move = "a";
            if(this.animateState.name!='ive_move_left')this.animateState = this.anim.play('ive_move_left');
        }
        else if(this.dDown){
            this.preDown_move = "d";
            this.playerSpeed_x = 300;
            this.playerSpeed_y = 0;
            if(this.animateState.name!='ive_move_right')this.animateState = this.anim.play('ive_move_right');
        }
        else if(this.wDown){
            this.playerSpeed_x = 0;
            this.playerSpeed_y = 300;
            this.preDown_move = "w";
            if(this.animateState.name!='ive_move_forward'){
                this.animateState = this.anim.play('ive_move_forward');
            }
        }
        else if(this.sDown){
            this.preDown_move = "s";
            this.playerSpeed_x = 0;
            this.playerSpeed_y = -300;
            if(this.animateState.name!='ive_move_backward')this.animateState = this.anim.play('ive_move_backward');
        }
        else{
            this.playerSpeed_x = 0;
            this.playerSpeed_y = 0;
            if(this.preDown_move=="a")this.animateState = this.anim.play('ive_idle_after_left');
            else if(this.preDown_move=="d")this.animateState = this.anim.play('ive_idle_after_right');
            else if(this.preDown_move=="w")this.animateState = this.anim.play('ive_idle_after_forward');
            else if(this.preDown_move=="s")this.animateState = this.anim.play('ive_idle_after_backward');
            else this.animateState = this.anim.play('ive_idle_after_backward');
        }

        this.node.x += this.playerSpeed_x * dt;  //move player
        this.node.y += this.playerSpeed_y * dt;  //move player
    }
    update(dt)
    {
        this.playerMovement(dt);
    }
}